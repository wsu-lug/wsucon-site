---
layout: default
title: Sponsors
---
<p>We strive to make WSUCon a completely free event to all of our participants. Without the hard work of the Linux Users' Group and the generous donations of all of our sponsors, food, prizes, and other awesome goodies just wouldn't be possible.</p>

<p>Our event comes at a considerable cost, we are asking for your help through sponsoring the event! There are many ways to sponsor WSUCon, you could donate:</p>
<ul>
    <li>Your time that can be used to help with the planning and execution of WSUCon.</li>
    <li>Prizes for tournaments, raffles, etc...</li>
    <li>A meal for WSUCon so we don't starve during this amazing 24-hour event.</li>
    <li>Money that can go to any one of these various options (and equipment) depending on the needs of the Con.</li>
</ul>
<h4>Supporter: $100</h4>
<p>This sponsorship level comes with a small logo on our website.</p>
<h4>Bronze: $500</h4>
<p>This sponsorship level comes with a small logo on both our website and posters.</p>
<h4>Silver: $1,000</h4>
<p>This sponsorship level comes with a bigger logo on our posters, fliers, and website.</p>
<h4>Gold: $2,000+</h4>
<p>This sponsorship level comes with a large logo on our posters, fliers, website, and shirts!</p>


<h3>WSUCon 2020 Sponsors:</h3>
<div class="row">
    <div class="col-md-12">
        <img style="background-color: gray" src="static/assets/sponsors/2020/cocacola.png" />
        <h5>Coca Cola</h5>
    </div>
    <div class="col-md-12">
        <img style="background-color: gray" src="static/assets/sponsors/2020/tespa.png" />
        <h5>Tespa</h5>
    </div>
    <div class="col-md-12">
        <img style="background-color: gray" src="static/assets/sponsors/2020/steel.png" />
        <h5>Steel Series</h5>
    </div>
    <div class="col-md-12">
        <img style="background-color: gray" src="static/assets/sponsors/2020/rog.png" />
        <h5>Republic of Gamers</h5>
    </div>
    <div class="col-md-12">
        <img style="background-color: gray" src="static/assets/sponsors/aswsu.png" />
        <h5>Associated Students of Washington State University</h5>
    </div>
    <div class="col-md-12">
        <img class="sponsor-image" src="static/assets/sponsors/lug.png" />
        <h5>WSU Linux Users' Group</h5>
    </div>
</div>
<h3>Our amazing organizers &amp; volunteers:</h3>
<p>The following people have donated an great deal of their time in order to make WSUCon awesome</p>
<ul>
    <li>Andrew Bates &amp; friends</li>
    <li>Mathew Merrick</li>
    <li>Rachel Forbes</li>
    <li>Mike Berger</li>
    <li>Eliza Dolecki</li>
    <li>Jacob Krahling</li>
    <li>Charlie Hanacek</li>
    <li>Jackson Peven</li>
    <li>Connor Wool</li>
    <li>Kenneth Eversole</li>
    <li>Andrew Lytle</li>
    <li>Erik Ostrom</li>
    <li>WSU Voiland College of Engineering and Architecture IT</li>
    <li>WSU Information Technology Services</li>
</ul>
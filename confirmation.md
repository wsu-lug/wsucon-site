---
layout: default
regenerate: true
title: Registration Confirmation
---


<!-- Call to Action Section -->
<div class="well">
    <div class="row">
        <div class="col-md-8">
            <p>Join the community.  Help keep WSUCon free by donating to our GoFundMe for WSUCon 2020.</p>
        </div>
        <div class="col-md-4">
            <a class="btn btn-lg btn-default btn-block" href="https://www.gofundme.com/WSUConXV">Donate Today</a>
        </div>
    </div>
</div>
<!-- Content Row -->
<div class="row">
    <div class="col-lg-12">
        <h3>Thank you for registering for WSUCon 2020!</h3>
        <div style="width:195px; text-align:center;" ><iframe  src="https://www.eventbrite.com/countdown-widget?eid=30563087016" frameborder="0" height="379" width="195" marginheight="0" marginwidth="0" scrolling="no" allowtransparency="true"></iframe><div style="font-family:Helvetica, Arial; font-size:12px; padding:10px 0 5px; margin:2px; width:195px; text-align:center;" ><a class="powered-by-eb" style="color: #ADB0B6; text-decoration: none;" target="_blank" href="http://www.eventbrite.com/">Powered by Eventbrite</a></div></div>            </div>
</div>

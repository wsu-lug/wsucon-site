---
layout: default
title: WSUCraft
---
<h2>Active</h2>
<h5>Mumble (mum)</h5>
<ul>
    <li>Connect: mumble.wsucraft.com</li>
</ul>

<h5>Minecraft (mc)</h5>
<ul>
    <li>Connect: minecraft.wsucraft.com:25567</li>
</ul>

<h2>Currently inactive</h2>
<h5>Feed The Beast (ftb)</h5>
<ul>
    <li>Connect: ftb.wsucraft.com:25566</li>
    <li>Dynmap: ftb.wsucraft.com/map (8124)</li>
</ul>

<h5>StarBound (sb)</h5>
<ul>
    <li>Connect: starbound.wsucraft.com</li>
</ul>
<h5>Terraria (ter)</h5>
<ul>
    <li>Connect: terraria.wsucraft.com</li>
    <li>Port: 7777</li>
</ul>

<h5>Other Things</h5>
<ul>
    <li>reddit.com/wsucraft</li>
    <li>The short names (mum, mc, sb, ter) should work for connecting to games.</li>
</ul>


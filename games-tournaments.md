---
title: Games and Tournaments
layout: default
---
<p>WSUCon is host to many different types of games. WSUCon isn't really just a LAN party, it's a complete social gaming event. We have 2 different areas set up for all sorts of gaming and we've got tournaments planned that include computer and console games! Participation in WSUCon is based on what you want to do. You can participate in tournaments, have a blast at just gaming by yourself or with friends, or just enjoy the demonstrations and lively atmosphere. It's all up to you!</p>
<div class="well">

<h3>Official Tournaments and Schedule</h3>
<p>You must be registered to participate in official/prized tournaments. Get excited for trophies! Everyone is welcome and encouraged to also participate in unofficial ad-hoc tournaments.</p>

{% capture schedule %}{% include schedule.html %}{% endcapture %}
{{ schedule | markdownify }}
</div>

<h3>What games are going to be played:</h3>
<h5>Computer Games</h5>
<ul>
    <li>Borderlands</li>
    <li>Terraria</li>
    <li>League of Legends</li>
    <li>Witcher 3</li>
    <li>Dark Souls 3</li>
    <li>BioShock</li>
    <li>OpenTTD</li>
    <li>Grand Theft Auto V</li>
    <li>Fortnite</li>
    <li>Minecraft</li>
    <li>Halo 1 CE</li>
    <li>Lin City</li>
    <li>Age of Empires II</li>
    <li>Starbound</li>
    <li>Portal</li>
    <li>Mini Metro</li>
    <li>Starwhal</li>
    <li>Runescape?!?</li>
    <li>Warsow</li>
    <li>NBA 2K</li>
    <li>Rainbow 6 Siege</li>
    <li>Destiny 2</li>
    <li>And more...</li>
</ul>
<h5>Console Games</h5>
<ul>
    <li>Super Smash Bros: Melee</li>
    <li>Smash Ultimate</li>
    <li>Smash 4</li>
    <li>Red Dead Redemption</li>
    <li>Spider Man</li>
    <li>Celeste</li>
    <li>Fortnite</li>
    <li>Super Mario Party</li>
    <li>Monser Hunter: World</li>
    <li>God of War</li>
    <li>And more...</li>
</ul>
<h5>Card Games</h5>
<ul>
    <li>Cards Against Humanity</li>
    <li>Magic: The Gathering</li>
    <li>Dominion</li>
    <li>Whatever you might be willing to bring!</li>
</ul>
<h5>Board Games</h5>
<ul>
    <li>Settlers of Catan</li>
    <li>5 Straight</li>
    <li>Risk</li>
    <li>Monopoly</li>
    <li>Whatever you might be willing to bring!</li>
</ul>
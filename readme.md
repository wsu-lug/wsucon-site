# WSUCon
[![Codeship Status for WSU-LUG/wsucon](https://app.codeship.com/projects/15c60710-e52d-0132-b884-266c7b4e6c8b/status?branch=master)](https://app.codeship.com/projects/81971)
[![Code Climate](https://codeclimate.com/github/WSU-LUG/wsucon/badges/gpa.svg)](https://codeclimate.com/github/WSU-LUG/wsucon)
[![Issue Count](https://codeclimate.com/github/WSU-LUG/wsucon/badges/issue_count.svg)](https://codeclimate.com/github/WSU-LUG/wsucon)

Repository for the [WSUCon Website](http://wsucon.wsu.edu/).
* [Bootstrap Base](https://github.com/BlackrockDigital/startbootstrap-modern-business)
	- based on the MIT License
* [Unstyled Bootstrap](https://startbootstrap.com/template-categories/unstyled/)

## Breakdown of structure
```
├── 404.md # 404 page, not currently in use
├── _config.yml # contains global configuration options including WSUCon year and other information that appears all over the site
├── confirmation.md # thanks users for registering for WSUCon
├── contact.md # contact info of wsucon main organizers
├── css # stylesheets
├── Dockerfile # describes how the containerized version of this site works
├── faq.md
├── font-awesome # contains font-awesome library
├── games-tournaments.md # includes information on selection of games at WSUCon. Also contains the schedule partial page
├── Gemfile # ruby dependencies
├── Gemfile.lock # ruby dependencies (don't edit this usually)
├── _includes # partials that appear in multiple spots across the site for DRY
│   ├── analytics.html
│   ├── footer.html
│   ├── head_matter.html
│   ├── navbar.html
│   ├── photo_carousel.html
│   ├── quick_datetime.html
│   ├── quick_location.html
│   ├── registration_banner.html
│   └── schedule.html
├── index.md # main page
├── js # js dependencies
│   ├── bootstrap.js
│   ├── bootstrap.min.js
│   ├── contact_me.js
│   ├── gallery.js
│   ├── jqBootstrapValidation.js
│   └── jquery.js
├── _layouts # layouts for different pages across the site
│   ├── default.html
│   ├── gallery.html
│   ├── home.html
│   └── registration.html
├── _old # pages that aren't in use at the moment
│   ├── csgo.html
│   ├── full-width.html
│   └── winterwonderlan.html
├── pictures.md # photo gallery from past wsucons
├── readme.md # (you are here)
├── registration.md # registration page with eventbrite iframe'd
├── _scripts # handy development scripts
│   ├── dbuild.sh # build the production container
│   └── start_development_server.sh # start a development server that watches files and automatically recompiles the site
├── sponsors.md # sponsorship information
├── static # static pages (NOT COMPLETE -- event photos are hosted on lugwww1 rather than in this repo due to file sizes. See lugwww1.eecs.wsu.edu under /home/shared/webserver-proxy for more information)
│   ├── assets # brand stuff goes in here
│   │   ├── backgrounds
│   │   │   ├── 06d6cac1.png
│   │   │   ├── 39ba1ec4.png
│   │   │   ├── cubes-3731993f.png
│   │   │   ├── cubes-47050a87.png
│   │   │   ├── eschereaque-1db8c6ec.png
│   │   │   ├── inflicted-b1907cf0.png
│   │   │   ├── pattern-alhi.png
│   │   │   └── shattered-5e314832.png
│   │   ├── banner
│   │   │   ├── banner_bg3.png
│   │   │   ├── banner_bgl3.png
│   │   │   └── banner_bgr3.png
│   │   ├── logos
│   │   │   ├── wsucon-3D-dark.png
│   │   │   ├── wsucon-3D-light.png
│   │   │   ├── wsucon-3D-red.png
│   │   │   ├── wsucon-dark.svg
│   │   │   ├── wsucon-light.svg
│   │   │   └── wsucon-light.svg.old
│   │   ├── social
│   │   │   └── steam-logo.png
│   │   ├── sponsors
│   │   │   ├── act-on.gif
│   │   │   ├── afschuld2.jpg
│   │   │   ├── afschuld.jpg
│   │   │   ├── aswsu.png
│   │   │   ├── branding2.png
│   │   │   ├── ComputerCrazy.png
│   │   │   ├── ednetics.gif
│   │   │   ├── eecs.png
│   │   │   ├── ggxylogo.png
│   │   │   ├── gold-sponsors.png
│   │   │   ├── hodgins.png
│   │   │   ├── league_of_legends.png
│   │   │   ├── lug.png
│   │   │   ├── official-sponsors.png
│   │   │   ├── rha.png
│   │   │   ├── seb.jpg
│   │   │   ├── seb.png
│   │   │   ├── silver-sponsors.png
│   │   │   ├── ssb_wsu.png
│   │   │   └── VGHLogoBlue1024x768.png
│   │   ├── tournaments
│   │   │   ├── wsuconxi
│   │   │   │   ├── leagueoflegends-b.png
│   │   │   │   ├── leagueoflegends.png
│   │   │   │   ├── starcraft-ii-b.png
│   │   │   │   ├── starcraft-ii.png
│   │   │   │   ├── teamfortress2-b.png
│   │   │   │   └── teamfortress2.png
│   │   │   └── wsuconxii
│   │   │       ├── leagueoflegends.png
│   │   │       ├── starcraft-ii.png
│   │   │       └── triathalon.png
│   │   ├── WSUCon-Map-HD.jpg
│   │   ├── WSUCon-Schedule-HD.jpg
│   │   └── WWLPoster2018.jpg
│   └── resources
│       └── css
│           └── override.css # this is where custom CSS goes
└── wsucraft.md # information on LUG-hosted game services

```

# This will all be changed. Hold on to your hats.

## Major TODOs

- refine & update logo
- menu bars available with scrolling
- quick to top scrolling button

# Future Plans

Dependencies for future builds include looking into:

### Overall platforms

1. Code: https://gitlab.com/wsu-lug/wsucon-site/
2. Deploy: https://codeship.acom/
5. Host: http://wsucon.wsu.edu/
6. Pretty: http://fortawesome.github.io/Font-Awesome/

### Dev dependencies

1. git
2. docker


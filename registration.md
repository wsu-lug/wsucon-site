---
layout: registration
regenerate: true
title: Registration
registration_info: It is important to pre-register for WSUCon "seats" as well as our capped tournaments like Super Smash Bros. We absolutely accept walk-ins, but we cannot guarantee seats once registration is full.
eventbrite_ticket_link: https://www.eventbrite.com/tickets-external?eid=90463740483&ref=etckt
---



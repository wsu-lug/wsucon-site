---
layout: default
regenerate: true
title: Contact Information
---
If you have questions about WSUCon that haven't been addressed on our FAQs page, please drop us a line. 
Two primary WSU Registered Student Organizations (RSOs) put on WSUCon: the [Linux Users' Group](https://lug.wsu.edu) and [WSU Esports](https://www.facebook.com/wsuesports/). Depending on your question, people from one organization could be better at answering it than the other. When in doubt, feel free to email them both.

For technology and logistics questions, please contact Linux Users' Group president Charlie Hanacek at charlie.hanacek (at) wsu.edu

For tournament and game questions, please contact WSU Esports president Bobby Belter at robert.belter (at) wsu.edu
